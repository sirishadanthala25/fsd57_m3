import { Component,OnInit} from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent implements OnInit{
id: number;
name: string;
avg: number;
 address: any;
 hobbie:any;
constructor(){
// alert("Constructor invoked...");
this.id=101;
this.name='Sirisha';
this.avg=45.34;

this.address={streetNo:101,city:'Hyderabad',state:'Telangana'};

this.hobbie=['sleeping','eating','watchingTv','Listening music'];

}


ngOnInit(){
  alert("ng OnInit invoked...");
}
}
