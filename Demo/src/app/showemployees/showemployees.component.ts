import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent implements OnInit {
  employees = [
    { empId: 1, empName: 'Sirisha', salary: 50000, gender: 'Female', doj: '2022-01-01', country: 'INDIA', emailId: 'sirishadanthala@gmail.com',experience:'2022-01-01' },
    { empId: 2, empName: 'Srilatha', salary: 56000, gender: 'Female', doj: '2023-04-21', country: 'USA', emailId: 'srilatha@gmail.com',experience:'2023-04-21'  },
    { empId: 3, empName: 'Vireesh', salary: 39000, gender: 'Male', doj: '2022-05-31', country: 'CANADA', emailId: 'vireesh@gmail.com',experience: '2022-05-31'},
    { empId: 4, empName: 'Mamatha', salary: 59000, gender: 'Female', doj: '2020-05-01', country: 'GERMANY', emailId: 'mamatha@gmail.com',experience: '2020-05-01' },
    { empId: 5, empName: 'Reethika', salary: 54000, gender: 'Female', doj: '2019-07-11', country: 'JAPAN', emailId: 'reethikadanthala@gmail.com',experience: '2019-07-11'}
  ];
  ngOnInit(){
    
  }

}
