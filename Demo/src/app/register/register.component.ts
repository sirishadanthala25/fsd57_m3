// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrl: './register.component.css'
// })
// export class RegisterComponent implements OnInit {
    
//   constructor() {
//   }

//   ngOnInit() {
//   }
 

//   registerSubmit(regForm: any) {
//     console.log(regForm);
//   }

// }
import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {
  
  empName: any;
  salary: any;
  gender: any;
  doj: any;
  country: any;
  phoneNumber: any;
  emailId: any;
  password: any;
  
  constructor(private router: Router, private toastr: ToastrService) {
  }

  ngOnInit() {
  }

  submit() {
    console.log("EmpName: " + this.empName);
    console.log("Salary: " + this.salary);
    console.log("Gender: " + this.gender);
    console.log("DateOfJoin: " + this.doj);
    console.log("Country: " + this.country);
    console.log("PhoneNumber: " + this.phoneNumber);
    console.log("Email-Id: " + this.emailId);
    console.log("Password: " + this.password);
  }

  registerSubmit() {
    const newEmployee = {
      empId: this.generateEmpId(), 
      empName: this.empName,
      salary: this.salary,
      gender: this.gender,
      doj: this.doj,
      country: this.country,
      phoneNumber: this.phoneNumber,
      emailId: this.emailId,
      password: this.password
    };

  
    console.log(newEmployee);

   
    this.toastr.success('Registration Successful!', 'Success', {
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-top-right',
      tapToDismiss: false,
      timeOut: 5000,
    });

    
    this.router.navigate(['login']);
  }

  private generateEmpId(): number {
   
    return Math.floor(Math.random() * 1000) + 1;
  }
}
